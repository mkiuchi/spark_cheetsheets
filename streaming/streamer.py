#!/usr/bin/env python3

import json
import socket
from datetime import datetime
import time

def genData():
    now = datetime.now()
    dataone = "aaa"
    eventtime = now.strftime('%s')
    dat = {'eventtime': int(eventtime), 'dataone': dataone, 'ts': int(now.strftime('%s'))}
    return json.dumps(dat)+'\n'

#while True:
#    print(genData())
#    time.sleep(1)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(('127.0.0.1', 9999))
    s.listen(1)
    while True:
        conn, addr = s.accept()
        with conn:
            while True:
                conn.sendall(genData().encode('utf-8'))
                time.sleep(1)
