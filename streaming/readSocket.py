from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()
sc = spark.sparkContext

from pyspark.sql.types import StructType, StructField, StringType, IntegerType

schema = StructType([
    StructField("eventtime", IntegerType(), True),
    StructField("dataone", StringType(), True),
    StructField("ts", IntegerType(), True)
])

lines = (spark
        .readStream
        .format('socket')
        .option('host', 'localhost')
        .option('port', 9999)
        .load())

from pyspark.sql.functions import col, from_json, from_unixtime

query = (lines
        .select(
            from_json(col("value"), schema).alias('json'))
        .select("json.*")
        .withColumn("et", from_unixtime("eventtime", format='YYYY-MM-dd HH:mm:ss'))
        .writeStream
        .outputMode('append')
        .format('console')
        .start())

query.awaitTermination()
