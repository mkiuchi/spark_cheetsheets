#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()
sc = spark.sparkContext


# In[ ]:

from pyspark.sql.types import StructType, StructField, StringType, IntegerType

schema = StructType([
    StructField("eventtime", IntegerType(), True),
    StructField("dataone", StringType(), True),
    StructField("ts", IntegerType(), True)
])

lines = (spark
        .readStream
        .format('socket')
        .option('host', 'localhost')
        .option('port', 9999)
        .load())

from pyspark.sql.functions import col, from_json, from_unixtime, window, to_timestamp

query = (lines
        .select(
            from_json(col("value"), schema).alias('json'))
        .select("json.*")
        .withColumn("ts", col("ts").cast("timestamp"))
        .withColumn("et", from_unixtime("eventtime", format='YYYY-MM-dd HH:mm:ss'))
        .withWatermark("ts", "15 seconds")
        .groupBy("dataone",
           window(col("ts"), "10 seconds", "5 seconds"))
        .count()
        .writeStream
        .outputMode('complete')
        .format('console')
        .option('truncate', False)
        .start())

query.awaitTermination()


# In[ ]:




